#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# get the logged in username and the path to the CCC directory
user="$(logname)"

#get user's home directory
if [ $user = "root" ]; then
	home="/root"
else
	home="$(cat /etc/passwd | grep $user | cut -d':' -f 6)"
fi

if [[ $1 == "--purge" ]]; then
	rm -rf $home/.gitbaby
fi

#deletes the git function out of .bashrc
sed -i "/git() {/,/}/d" $home/.bashrc

if rm /usr/local/bin/git-save &&
   rm /usr/local/bin/git-ls &&
   rm /usr/local/bin/git-cd &&
   rm /usr/local/bin/git-delete &&
   rm /usr/local/bin/git-sync; then
	echo "Uninstalled successfully!"
else
	rm /usr/local/bin/git-save
	rm /usr/local/bin/git-ls
	rm /usr/local/bin/git-cd
	rm /usr/local/bin/git-delete
	rm /usr/local/bin/git-sync
	echo "Something went wrong; most probably some of the installed files were missing. I removed what I could find."
fi
