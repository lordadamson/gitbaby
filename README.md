# gitbaby
A git extension that eases the access to the git repos you use frequently. </br>

## Installation
* Clone this repo: `git clone https://github.com/lordadamson/gitbaby`
* cd into it: `cd gitbaby`
* Run the installation script: `sudo ./install.sh`
* Source your .bashrc: `. ~/.bashrc`

## Usage
#### `git save`
`cd` into any of the git repos that you use frequently. let's say it's located in `~/projects/horse-fly/` and then run: `git save`

#### `git cd <name_of_repo>`
Now your repo has been saved and you can get back to it easily. so let's say you open your terminal and the default working directory is `~`, you can simply run `git cd horse` and it's gonna take you to `~/projects/horse-fly/`. Neat huh? Notice that you didn't have to specify the whole directory name, you just typed a part of the name that is distinguishable from other saved git repos.

#### `git ls`
You can list all of your saved git repos by: `git ls`

#### `git delete <name_of_repo>`
You can delete a git repo by: `git delete horse` (Note that this will not erase the actual repository, but will just remove the gitbaby reference to it so you won't be able to `git cd` into any more and you won't see it listed in `git ls`.

#### `git sync <your commit message here>`
I don't know about you, but 85% of my usage to git is `git add .` `git commit -m "some commit message"` `git pull` `git push`. Aaaand that's exactly what the sync sub-command is for. it does all that for you, all you have to do is `git sync "my commit message"` and it takes care of the rest for you :))

BTW let me know if the git sync should be seperated in a different package. I know it's unrelated but I just collected all my shortcuts and put it in the same place.

### Development
Okay so it turns out that you can extend git with custom sub-commands; all you have to do is create an executable file with the name `git-whatever` (replace whatever with the name of your sub-command) and place that executable somewhere in your PATH, and git will automatically detect it! And that's exactly what we're doing here, we have `git-save`, `git-ls`, `git-ls`, `git-cd` and `git-delete`.

Our git sub-commands are fairly simple and straight forward bash scripts. Basically what we do is that we place the saved repos in `~/.gitbaby/gitrepos` and we read from that file to `git-ls` and `git-cd` and we write to it from `git-save` and `git-delete`. So yeah nothing's fancy going on here. We do have things to be done though. checkout the **TODO** section below.

## TODO:
* [x] Change the location of the gitRepos file somewhere convenient like `~/.gitRepos/gitRepos` for example.
* [ ] Create a global hook that calls the init script when the git init command is issued. (Thanks [Ahmad Fatoum](https://www.facebook.com/athreef) for the idea).
* [x] Create `git-ls` and `git-cd` commands and have them installed in the PATH. (Thanks [Ahmad Fatoum](https://www.facebook.com/athreef) for the idea).
* [x] We need a removal mechanism, to remove deleted repos from the gitRepos file. </br>
* [ ] We also need to validate inputs before the script can execute.
* [ ] Create Debian package.
* [ ] Create RPM Package.
* [ ] Create Arch Package.
