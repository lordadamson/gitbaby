#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# get the logged in username and the path to the CCC directory
user="$(logname)"

#get user's home directory
if [ $user = "root" ]; then
	home="/root"
else
	home="$(cat /etc/passwd | grep $user | cut -d':' -f 6)"
fi

mkdir -p $home/.gitbaby
chown $user:$user $home/.gitbaby

# get the path of the script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cat $DIR/gitfunc >> $home/.bashrc

cp $DIR/git-* /usr/local/bin/

if [[ $? == 0 ]]; then
	echo "success"
else
	echo "something went wrong :("
fi
